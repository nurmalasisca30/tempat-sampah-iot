package com.example.maps;

import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng GedungJTIPolije = new LatLng(-8.15755990549073, 113.72287788468475);
        mMap.addMarker(new MarkerOptions().position(GedungJTIPolije).title("Tong Sampah 1"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(GedungJTIPolije));

        // Add a marker in Sydney and move the camera
        LatLng doublewayunej = new LatLng(-8.163811530297579, 113.71306724846363);
        mMap.addMarker(new MarkerOptions().position(doublewayunej).title("Tong Sampah 2"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(doublewayunej));

        // Add a marker in Sydney and move the camera
        LatLng kejaksaannegerijember = new LatLng(-8.17132585209669, 113.72161813080311);
        mMap.addMarker(new MarkerOptions().position(kejaksaannegerijember).title("Tong Sampah 3"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kejaksaannegerijember));
    }
}
